# Project Name
Automation Scripts for Amazon.in

A brief description of what this project:

This project contains a set of automated tests for Amazon.in. 
The tests are built using Selenium, POM and pytest frameworks.

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)

## Installation
To install the dependencies, run the following command:
pip install 

1. Clone this repository to your local machine.
2. Python installed on your system (version 3.x.x).
3. Selenium package installed (version 4.x.x).
4. pytest package installed (version 6.x.x).
5. ChromeDriver installed on your system.
6. Google Chrome browser installed on your system.

## Usage

Running the tests
1. Open the terminal/command prompt in the directory containing the test script and the ChromeDriver executable.
2. Run the following command to execute all the tests:
3. pytest -v
4. This will launch the Google Chrome browser and run all the tests.
5. After the tests are completed, the browser will be closed automatically.
6. Note: If you want to run a specific test case, use the following command:
7. pytest -v -k "test_case_name"
8. Replace "test_case_name" with the name of the test case you want to run.
9. 
## scripts description 
Test Scenarios
The automation tests cover the following scenarios:

1. Login Test: This test logs in to the Amazon website using a valid email and password and verifies that the login was successful.
2. All Menu Test: This test clicks the "hamburger" menu button and verifies that the "see all" button is visible.
3. See All Test: This test clicks the "see all" button and verifies that the "Car, Motorbike, Industrial" link is visible.
4. Car Motorbike Industrial Test: This test clicks the "Car, Motorbike, Industrial" link and verifies that the "All Car & Motorbike Products" link is visible.
5. Car Accessories Test: This test clicks the "All Car & Motorbike Products" link and verifies that the "Car Accessories" radio button is visible and clickable.
6. Click Radio Button Test: This test clicks the "Car Accessories" radio button and verifies that the prices of the products displayed are less than 500.
7. Verify Prices Test: This test verifies that the prices of the products displayed are less than 500.
8. Add to Cart Test: This test adds the first four products displayed to the cart and verifies that they are added successfully.
9. Select Address Test: This test selects the default address for delivery and verifies that it is selected successfully.
10. Return back to shopping cart : This test covers the window handles and return back to shopping cart.
11. delete all the items in the cart : This test deletes all the items in the shopping cart where delete buttons are the clickable or not.
12. validate “Your Amazon Cart is Empty” message : This test validates shopping cart is empty after deleting the items in the cart and validates the message"Your Amazon Cart is Empty".
13. Executing all the testcases driver will close the browser.


## License

Include any relevant license information for the project.
