import time
from selenium import webdriver
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.service import Service
from behave import given, when, then
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC



s = Service('googlechrome.dmg')
driver = webdriver.Chrome(service=s)


class Login():

    def __init__(self, driver):
        self.driver = driver
    def launch_browser(self):
        driver.get('https://www.amazon.in/')
        driver.maximize_window()

    def maximize_window(self):
        self.driver.maximize_window()
    def Homepage_signIn_btn(self):
        xpath = r'nav-link-accountList'
        self.driver.find_element(By.ID, xpath).click()

    def get_email(self,email):
        xpath=r'//input[@type="email"]'
        self.driver.find_element(By.XPATH,xpath).clear()
        self.driver.find_element(By.XPATH,xpath).send_keys(email)

    def continue_button(self):
        xpath = r'//input[@class="a-button-input"]'
        self.driver.find_element(By.XPATH,xpath).click()

    def get_pwd(self,pwd):
        xpath=r'//input[@type="password"]'
        self.driver.find_element(By.XPATH,xpath).clear()
        self.driver.find_element(By.XPATH,xpath).send_keys(pwd)

    def signin_btn(self):
        xpath=r'//input[@id="signInSubmit"]'
        self.driver.find_element(By.XPATH, xpath).click()

LOGINPAGE=Login(driver)
LOGINPAGE.launch_browser()
LOGINPAGE.maximize_window()
LOGINPAGE.Homepage_signIn_btn()
LOGINPAGE.get_email("Veeresh090@gmail.com")
LOGINPAGE.continue_button()
LOGINPAGE.get_pwd("Veeresh090@")
LOGINPAGE.signin_btn()
