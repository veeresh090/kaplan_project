import time

import pytest
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

@pytest.fixture(scope="module")
def setup():
    s = Service('googlechrome.dmg')
    driver = webdriver.Chrome(service=s)
    driver.get('https://www.amazon.in/')
    driver.maximize_window()
    yield driver
    driver.quit()

def test_login(setup):
    driver = setup
    # Click on the SignIn button on the Amazon homepage
    driver.find_element(By.XPATH,LOGINPAGE.account).click()

    # Enter email and click Continue
    email_field = driver.find_element(By.XPATH, LOGINPAGE.username)
    email_field.clear()
    email_field.send_keys("Veeresh090@gmail.com")
    continue_button = driver.find_element(By.XPATH, LOGINPAGE.continue_btn)
    continue_button.click()

    # Enter password and click Sign In
    pwd_field = driver.find_element(By.XPATH, LOGINPAGE.password)
    pwd_field.clear()
    pwd_field.send_keys("Veeresh090@")
    signin_button = driver.find_element(By.XPATH, LOGINPAGE.signin_btn)
    signin_button.click()

    # Verify login
    assert "Amazon Sign In" in driver.title
def test_Allmenu(setup):
    driver = setup
    driver.find_element(By.ID, "nav-hamburger-menu").click()

def test_seeall(setup):
    driver = setup
    shop_by_category = driver.find_element(By.XPATH, '//div[contains(text(),"see all")]')
    driver.implicitly_wait(5)
    driver.execute_script("arguments[0].scrollIntoView();", shop_by_category)
    time.sleep(3)
    shop_by_category.click()

def test_car_motorbike_industrial(setup):
    driver = setup
    driver.execute_script("window.scrollBy(0, 1000)")
    car_motorbike_industrial = driver.find_element(By.XPATH, "//a/div[contains(text(),'Car, Motorbike, Industrial')]")
    driver.implicitly_wait(5)
    time.sleep(3)
    car_motorbike_industrial.click()

def test_car_accessories(setup):
    driver = setup
    car_accessories = driver.find_element(By.XPATH, "//a[contains(text(),'All Car & Motorbike Products')]")
    driver.implicitly_wait(5)
    time.sleep(3)
    car_accessories.click()


def test_click_radio_button(setup):
    driver=setup
    xpath=r'//*[@id="s-refinements"]/div[10]/ul/li[1]/span/a/span'
    driver.implicitly_wait(5)
    radio_btn=driver.find_element(By.XPATH , xpath)
    driver.execute_script("arguments[0].scrollIntoView();", radio_btn)
    radio_btn.click()

def test_verify_pricess(setup):
    driver=setup
    product_prices = driver.find_elements(By.XPATH, "//span[@class='a-price-whole' and number(text()) < 500]")
    for price in product_prices:
        assert int(price.text.replace(',', '')) < 500

def test_add_to_cart(setup):
    driver=setup
    global products
    products = []
    product_names = driver.find_elements(By.XPATH , '//h2[@class="a-size-mini a-spacing-none a-color-base s-line-clamp-4"]/a')
    product_prices = driver.find_elements(By.XPATH , "//span[@class='a-price-whole']")
    for i in range(4):
        product = {}
        product['name'] = product_names[i].text
        product['price'] = product_prices[i].text
        products.append(product)

    print(products)

    # Add all the 4 the items to the cart
    for i in range(4):
        # Click on the product name to go to the product page
        current_window = driver.current_window_handle
        product_names[i].click()
        window_handles = driver.window_handles
        for handle in window_handles:
            if handle != current_window:
                new_window = handle
                break
        driver.switch_to.window(new_window)

        # Add the product to the cart
        wait = WebDriverWait(driver, 10)
        add_to_cart_button = wait.until(
            EC.presence_of_element_located((By.XPATH, '//input[@name="submit.add-to-cart"]')))
        # Click the element
        driver.implicitly_wait(5)
        add_to_cart_button.click()
        # Close the new window or tab
        driver.close()

        # Switch back to the original window or tab
        driver.switch_to.window(current_window)
    driver.refresh()
    time.sleep(5)
    driver.execute_script('window.scrollBy(0, -500);')

    driver.find_element(By.XPATH, '//a[@id="nav-cart"]').click()
    driver.implicitly_wait(5)
    driver.find_element(By.XPATH, '//input[@value="Proceed to checkout"]').click()
    driver.implicitly_wait(5)

def test_select_address(setup):
    driver=setup
    driver.implicitly_wait(5)
    address = driver.find_element(By.XPATH, '//*[@id="address-list"]/div/div[1]/div/fieldset[1]/div')
    driver.implicitly_wait(5)
    if address.is_enabled():
        print("address is selected")
    else:
        address.click()
    driver.implicitly_wait(5)
    driver.find_element(By.XPATH, '(//input[@class="a-button-input"])[6]').click()
    time.sleep(5)

def test_validate_prices_in_payment_page(setup):
    driver=setup
    # validate the prices of the items in the payment page matches
    product_in_payment = driver.find_elements(By.CSS_SELECTOR, "div.a-row.a-spacing-mini.a-size-small")
    for product_names, product_prices in products:
        for item in product_in_payment:
            if product_names in item.text:
                assert product_prices in item.text, f"Price doesn't match for {product_names}"

def test_back_to_cart_page(setup):
    driver=setup
    driver.back()
    driver.back()
    driver.implicitly_wait(3)
    driver.refresh()
    assert "Amazon.in Shopping Cart" in driver.title

def test_delete_all_items_in_cart(setup):
    driver=setup
    delete_button_xpath = "//input[@value='Delete']"
    items_in_cart = driver.find_elements(By.XPATH, delete_button_xpath)
    if len(items_in_cart) > 0:
        while len(items_in_cart) > 0:
            time.sleep(5)
            item = items_in_cart[0]
            delete_button = driver.find_element(By.XPATH, delete_button_xpath)
            time.sleep(5)
            delete_button.click()
            WebDriverWait(driver, 5).until(EC.staleness_of(item))
            items_in_cart = driver.find_elements(By.XPATH, delete_button_xpath)
            driver.refresh()
            if len(items_in_cart) == 0:  # check if condition is satisfied
                break  # break out of the loop if condition is not satisfied
        


def test_validate_cart_empty(setup):
    driver=setup
    time.sleep(5)
    try:
        empty_cart_message = driver.find_element(By.XPATH, '//h1[@class="a-spacing-mini a-spacing-top-base"]')
        assert empty_cart_message.text == "Your Amazon Cart is empty."
        print("Empty cart message is validated successfully.")
    except AssertionError as e:
        print(f"Expected empty cart message not found: {e}")

    print("All Testcases are done")
