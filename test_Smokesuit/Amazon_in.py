from config.Settings import *

s = Service('googlechrome.dmg')
driver = webdriver.Chrome(service=s)
driver.get('https://www.amazon.in/')
driver.implicitly_wait(5)
driver.maximize_window()
driver.find_element(By.ID,"nav-link-accountList").click()
driver.maximize_window()
driver.find_element(By.XPATH,'//input[@type="email"]').send_keys("Veeresh090@gmail.com")
driver.implicitly_wait(5)
driver.find_element(By.XPATH,'//input[@class="a-button-input"]').click()
driver.implicitly_wait(5)
driver.find_element(By.XPATH,'//input[@type="password"]').send_keys("Veeresh090@")
driver.implicitly_wait(5)
driver.find_element(By.XPATH,'//input[@id="signInSubmit"]').click()

driver.find_element(By.ID, "nav-hamburger-menu").click()
driver.implicitly_wait(5)
shop_by_category = driver.find_element(By.XPATH, '//div[contains(text(),"see all")]')
driver.implicitly_wait(5)
shop_by_category.click()
car_motorbike_industrial = driver.find_element(By.XPATH, "//a/div[contains(text(),'Car, Motorbike, Industrial')]")
driver.implicitly_wait(5)
car_motorbike_industrial.click()
car_accessories = driver.find_element(By.XPATH, "//a[contains(text(),'All Car & Motorbike Products')]")
driver.implicitly_wait(5)
car_accessories.click()
driver.execute_script("window.scrollBy(0, 500);")
rs_500_filter = driver.find_element(By.XPATH, '//*[@id="s-refinements"]/div[10]/ul/li[1]/span/a/span')
driver.implicitly_wait(5)
rs_500_filter.click()


# Verify the items displayed in the page are under RS 500
product_prices = driver.find_elements(By.XPATH, "//span[@class='a-price-whole' and number(text()) < 500]")
for price in product_prices:
    assert int(price.text.replace(',', '')) < 500

# Store any 4 of the item’s names and prices in a variable
products = []
product_names = driver.find_elements(By.XPATH , '//h2[@class="a-size-mini a-spacing-none a-color-base s-line-clamp-4"]/a')
product_prices = driver.find_elements(By.XPATH , "//span[@class='a-price-whole']")
for i in range(4):
    product = {}
    product['name'] = product_names[i].text
    product['price'] = product_prices[i].text
    products.append(product)

print(products)

# Add all the 4 the items to the cart
for i in range(4):
    # Click on the product name to go to the product page
    current_window= driver.current_window_handle
    product_names[i].click()
    window_handles = driver.window_handles
    for handle in window_handles:
        if handle != current_window:
            new_window = handle
            break
    driver.switch_to.window(new_window)

    # Add the product to the cart
    wait = WebDriverWait(driver, 10)
    add_to_cart_button = wait.until(EC.presence_of_element_located((By.XPATH, '//input[@name="submit.add-to-cart"]')))
    # Click the element
    driver.implicitly_wait(5)
    add_to_cart_button.click()
    # Close the new window or tab
    driver.close()

    # Switch back to the original window or tab
    driver.switch_to.window(current_window)
driver.refresh()
time.sleep(5)
driver.execute_script('window.scrollBy(0, -500);')

driver.find_element(By.XPATH,'//a[@id="nav-cart"]').click()
driver.implicitly_wait(5)
driver.find_element(By.XPATH,'//input[@value="Proceed to checkout"]').click()
driver.implicitly_wait(5)


driver.implicitly_wait(5)
address=driver.find_element(By.XPATH,'//div[@class="a-row address-row list-address-selected"]')
driver.implicitly_wait(5)
if address.is_enabled():
    print("address is selected")
else:
    address.click()
driver.implicitly_wait(5)
driver.find_element(By.XPATH,'(//input[@class="a-button-input"])[6]').click()

# validate the prices of the items in the payment page matches
product_in_payment = driver.find_elements(By.CSS_SELECTOR, "div.a-row.a-spacing-mini.a-size-small")
for product_names, product_prices in products:
    for item in product_in_payment:
        if product_names in item.text:
            assert product_prices in item.text, f"Price doesn't match for {product_names}"

driver.back()
driver.implicitly_wait(3)
driver.refresh()

# delete all the items from the cart
delete_button_xpath = "//input[@value='Delete']"
items_in_cart = driver.find_elements(By.XPATH, delete_button_xpath)
if len(items_in_cart) > 0:
    while len(items_in_cart) > 0:
        time.sleep(5)
        item = items_in_cart[0]
        delete_button = driver.find_element(By.XPATH, "//input[@value='Delete']")
        time.sleep(5)
        delete_button.click()
        WebDriverWait(driver, 5).until(EC.staleness_of(item))
        items_in_cart = driver.find_elements(By.XPATH, delete_button_xpath)
else :
    print("All items are deleted")


# validate the message "Your Amazon Cart is Empty" message
try:
    empty_cart_message = driver.find_element(By.XPATH, '//h1[@class="a-spacing-mini a-spacing-top-base"]')
    assert empty_cart_message.text == " Your Amazon Cart is empty."
    print("Empty cart message is validated successfully.")
except AssertionError as e:
    print(f"Expected empty cart message not found: {e}")

print("All Testcases are done")

