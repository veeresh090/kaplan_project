class HOMEPAGE:
    ALLMENU_BTN = '//a[@id="nav-hamburger-menu"]'
    SEEALL_BTN = '//div[contains(text(),"see all")]'
    CAR_MOTORBIKE_INDUSTRY_BTN = "//a/div[contains(text(),'Car, Motorbike, Industrial')]"
    CAR_ACCESSARIES_BTN = "//a[contains(text(),'All Car & Motorbike Products')]"
    RADIO_BTN = '//*[@id="s-refinements"]/div[10]/ul/li[1]/span/a/span'
    VERIFY_PRICE = "//span[@class='a-price-whole' and number(text()) < 500]"
