class ADD_TO_CART:
    PRODUCT_NAME = '//h2[@class="a-size-mini a-spacing-none a-color-base s-line-clamp-4"]/a'
    PRODUCT_PRICE = "//span[@class='a-price-whole']"
    ADD_TO_CART_BTN = '//input[@name="submit.add-to-cart"]'
    LOCAL_BTN = '//a[@id="nav-cart"]'
    PROCEED_TO_CHECKOUT_BTN = '//input[@value="Proceed to checkout"]'
class ADDRESS:
    DEFAULT_ADDRESS = '//*[@id="address-list"]/div/div[1]/div/fieldset[1]/div'
    MANUAL_ADDRESS = '(//input[@class="a-button-input"])[6]'
class PRODUCTPAYMENT:
    PRODUCT_IN_PAYMENT = 'div.a-row.a-spacing-mini.a-size-small'
class DELETEITEMS:
    DELETE_ITEMS_CART = "//input[@value='Delete']"
class CARTISEMPTY:
    YOUR_CART_IS_EMPTY = '//h1[@class="a-spacing-mini a-spacing-top-base"]'